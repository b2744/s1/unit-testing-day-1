//express
const express = require("express");

//server
const app = express();

//port
const port = 5000;



//middleware
app.use(express.json());

const newUser = {
	firstName: 'John',
	lastName: 'Dela Cruz',
	age: 18,
	contactNumber: '09123456789',
	batchNumber: 151,
	email: 'john.delacruz@gmail.com',
	password: 'sixteencharacters'
}

// Activity Instructions: 
 /*
	1. Assert that the newUser firstname type is a string
	2. Assert that the newUser lastname type is a string
	3. Assert that the newUser firstname is not undefined
	4. Assert that the newUser lastname is not undefined
	5. Assert that the newUser age is atleast 18
	6. Assert that the newUser age type is a number
	7. Assert that the newUser contact number type is a string
	8. Assert that the newUser batch number type is a number
	9. Assert that the newUser batch number is not undefined
 */

//exporting newUser
module.exports = {
	newUser: newUser
}

app.listen(port, ()=> console.log(`Server is running at port ${port}`));