// require assert under chai
const {assert} = require('chai');

// importing newUser
const {newUser} = require('../index.js');


// describe and it are under mocha
//gives the structure to test suite.
describe('Test newUser object', ()=> {
	it('Assert newUser type is object',()=>{
		assert.equal(typeof(newUser), 'object')
	})
	it('Assert newUser.email is type string', ()=>{
		assert.equal(typeof(newUser.email), 'string')
	})
	it('Assert newUser.email is not undefined', ()=>{
		assert.notEqual(typeof(newUser.email), 'undefined')
	})
	it('Assert newUser.password type is string', ()=>{
		assert.equal(typeof(newUser.password),'string')
	})
	it('Assert that password length is 16 character long', ()=>{
		assert.isAtLeast(newUser.password.length, 16)
	})
	it('Assert newUser.firstName is a string', ()=>{
		assert.equal(typeof(newUser.firstName),'string')
	})
	it('Assert newUser.lastName is a string', ()=>{
		assert.equal(typeof(newUser.lastName),'string')
	})
	it('Assert newUser.firstName is not undefined', ()=>{
		assert.notEqual(typeof(newUser.firstName), 'undefined')
	})
	it('Assert newUser.lastName is not undefined', ()=>{
		assert.notEqual(typeof(newUser.lastName), 'undefined')
	})
	it('Assert that age is at least 18 years old', ()=>{
		assert.isAtLeast(newUser.age, 18)
	})
	it('Assert that age is a number', ()=>{
		assert.equal(typeof(newUser.age), 'number')
	})
	it('Assert that contact number is a string',()=>{
		assert.equal(typeof(newUser.contactNumber),'string')
	})
	it('Assert that batch number is a number', ()=>{
		assert.equal(typeof(newUser.batchNumber), 'number')
	})
	it('Assert that batch number is not undefined', ()=>{
		assert.notEqual(typeof(newUser.batchNumber), 'undefined')
	})
	



})


